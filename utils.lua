local varargs = {...}
local TCP = varargs[1]
irc = {}
irc.users = {}

function irc.sendraw(str)
  TCP:send(str .. "\n")
  print(">>: "..str)
end

function irc.send(dst, str)
  TCP:send("PRIVMSG " .. dst .." :" .. str .. "\n")
  print(dst .. "|->: "..str)
end

function irc.isOp(chn, usr)
  local op = false
  if irc.users[chn].ops[usr] then op = true end
  return op
end

function irc.isVoice(chn, usr)
  local voice = false
  if irc.users[chn].ops[usr] or irc.users[chn].vcs[usr] then voice = true end
  return voice
end

function irc.channelJoin(chn)
  irc.sendraw("JOIN " .. chn)
end

function irc.channelLeave(chn)
  irc.sendraw("PART " .. chn)
end

function string.split(self, pat)
  local t = {}
  local fpat = "(.-)" .. pat
  local last_end = 1
  local s, e, cap = self:find(fpat, 1)
  while s do
    if s ~= 1 or cap ~= "" then
     table.insert(t,cap)
    end
    last_end = e+1
    s, e, cap = self:find(fpat, last_end)
  end
  if last_end <= #self then
    cap = self:sub(last_end)
    table.insert(t, cap)
  end
  return t
end

function string.join(self, tab)
  return table.concat(tab, self)
end

function moduleSetup()
  print("**: Module table setup..")
  _MODULES         = {}
  _MODULES.Loaded  = {}
  
  _MODULES.Event               = {}
  _MODULES.Event.Command       = {}
  _MODULES.Event.Message       = {}
  _MODULES.Event.Mode          = {}
  _MODULES.Event.Nick          = {}
  _MODULES.Event.Join          = {}
  _MODULES.Event.Leave         = {}
  _MODULES.Event.Kick          = {}
  _MODULES.Event.Quit          = {}
  _MODULES.Event.BotJoinedChan = {}
  _MODULES.Event.BotStartup    = {}
  
  print(" *: Done.")
  return _MODULES
end