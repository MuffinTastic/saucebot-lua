-- // saucebot in lua // muffin 2015 // --

-- public so modules can access it
local socket = require "socket"
local tcp    = assert(socket.tcp())

config = assert(loadfile("config.lua"))()
print("**: Loaded config.")
loadfile("utils.lua")(tcp)


function loadModules()
  config = assert(loadfile("config.lua"))()
  print("**: Loaded config.")
  _MODULES = moduleSetup()
  
  print("**: Loading modules..")
  
  for _,module in ipairs(config.modules) do
    loaded_module, mod_or_err = pcall(assert, loadfile("modules/" .. module .. ".lua"))
    if loaded_module then
      Module = _MODULES.Event
      
      ranfunc, res_or_err = pcall(mod_or_err)
      if ranfunc then
        print(" *: Loaded module: " .. module)
        _MODULES.Loaded[module] = true
      else
        print("!!: Couldn't load module '" .. module .. "': " .. res_or_err)
      end
    else
      print("!!: Couldn't load module '" .. module .. "': " .. mod_or_err)
    end
  end
  
  print(" *: Done.")
end

print("**: Connecting to host " .. config.host .. " on port " .. config.port .. ".")
tcp:connect(config.host, config.port)

do
  local nk = config.nick
  local dk = config.desc
  
  irc.sendraw("USER "..nk.." "..nk.." "..nk.." :"..dk)
  irc.sendraw("NICK "..nk)
end

loadModules()

while true do
  local inp, sts, partial = tcp:receive()
  if sts == "closed" then break end  -- ' '.join(cnt[3:len(cnt)])[1:]   cnt[0][1:].split('!')
  
  -- message info stuffs
  local _jn = " "
  local cnt  = inp:split(" ")
  local chn = cnt[3]
  local msg = _jn:join({unpack(cnt, 4, #cnt)}):sub(2,#(_jn:join({unpack(cnt, 4, #cnt)})))
  local usr, ipa = unpack(cnt[1]:sub(2,#(cnt[1])):split("!"))
  
  local event = cnt[2]
  print("<-: "..(inp or partial))
  -- if event == "NICK" then print(usr,ipa,chn,msg) end
  
  if string.match(inp, ":"..config.nick.."(.*) MODE "..config.nick.."(.*)") then
    for _,chan in ipairs(config.channels) do
      irc.sendraw("JOIN "..chan)
    end
    
    for _,fnc in pairs(_MODULES.Event.BotStartup) do
      fnc()
    end
  end
  
  if string.match(inp, "PING :(.*)") then
    local pong = string.match(inp, "PING :(.*)")
    irc.sendraw("PONG "..pong)
  end
  
  if (cnt[2] == "353") and (chn == config.nick) then
    do
      local _users = msg:sub(2, -1):split(" :")
      irc.users[_users[1]] = _users[2]:split(" ")
      
      local _chan = irc.users[_users[1]]
        _chan.ops = {}
        _chan.vcs = {}
        _chan.amp = {}
        _chan.tld = {}
        _chan.all = {}
        
      
      for _,nick in ipairs(_chan) do
        if nick:sub(1,1) == "@" then 
          _chan.ops[nick:sub(2,-1)] = true
          nick = nick:sub(2,-1)
        end
        
        if nick:sub(1,1) == "+" then
          _chan.vcs[nick:sub(2,-1)] = true
          nick = nick:sub(2,-1)
        end
        
        if nick:sub(1,1) == "&" then
          _chan.amp[nick:sub(2,-1)] = true
          nick = nick:sub(2,-1)
        end
        
        if nick:sub(1,1) == "~" then
          _chan.tld[nick:sub(2,-1)] = true
          nick = nick:sub(2,-1)
        end
        
        _chan.all[nick] = true
        
        _chan[_] = nil
      end
    
      print("**: Opped in ".._users[1])
        for v in pairs(_chan.ops) do
          print(" *:    "..v)
        end
    end
  end

  if (cnt[2] == "366") and (chn == config.nick) then
    local _chn = cnt[4]
    
    for _,fnc in pairs(_MODULES.Event.BotJoinedChan) do
      fnc({channel = _chn})
    end
  end
  
  if event == "PRIVMSG" then
    local _pm = ((chn == config.nick) and true or false)
    local _args = {user = usr,ip = ipa,channel = (_pm and usr or chn),message = msg, pm=_pm}
    
    for cmd,fnc in pairs(_MODULES.Event.Command) do
      local _runningoutofnames = msg:split(" ")
      for k,v in pairs({unpack(_runningoutofnames, 2, #_runningoutofnames)}) do
        _args[k] = v
      end
      
      if _runningoutofnames[1] == config.prefix .. cmd then
        print("**: User " .. usr .. " has called command '" .. cmd .. "'.")
        
        fnc(_args)
      end
    end
    
    for _,fnc in pairs(_MODULES.Event.Message) do
      fnc({user = usr,ip = ipa,channel = chn,message = msg})
    end
  end

  if event == "MODE" then
    do
      local mode, rcp = unpack(_jn:join({unpack(cnt, 4, #cnt)}):split(" "))
      local mode_rec = ((mode:sub(1,1) == "+") and true or false)
      local mode_smb = mode:sub(2,-1)
      
      if mode_smb:find("o") then
        if mode_rec then
          irc.users[chn].ops[rcp] = true
        else
          irc.users[chn].ops[rcp] = nil
        end
      end
      
      if mode_smb:find("v") then
        if mode_rec then
          irc.users[chn].vcs[rcp] = true
        else
          irc.users[chn].vcs[rcp] = nil
        end
      end
      
      for _,fnc in pairs(_MODULES.Event.Mode) do
        fnc({user = usr,ip = ipa,channel = chn,mode = {raw = mode, receive = mode_rec, symbol = mode_smb}, recepient = rcp}) 
      end
    end
  end
  
  if event == "JOIN" then
    if not irc.users[chn] then
      irc.users[chn] = {}
      irc.users[chn].ops = {}
      irc.users[chn].vcs = {}
      irc.users[chn].amp = {}
      irc.users[chn].tld = {}
      irc.users[chn].all = {}
    end
    
    irc.users[chn].all[usr] = true
    
    for _,fnc in pairs(_MODULES.Event.Join) do
      fnc({user = usr,ip = ipa,channel = chn})
    end
  end
  
  if event == "PART" then
    for _,fnc in pairs(_MODULES.Event.Leave) do
      fnc({user = usr,ip = ipa,channel = chn,message = msg})
    end
    irc.users[chn].ops[usr] = nil
    irc.users[chn].vcs[usr] = nil
    irc.users[chn].all[usr] = nil
  end
  
  if event == "KICK" then
    local kcd, rsn = unpack(_jn:join({unpack(cnt, 4, #cnt)}):split(" :"))
    
    for _,fnc in pairs(_MODULES.Event.Kick) do
      fnc({user = usr, ip = ipa, channel = chn, kicked = kcd, reason = rsn})
    end
  end

  if event == "NICK" then
    for _,fnc in pairs(_MODULES.Event.Nick) do
      fnc({user = usr,ip = ipa,new = chn:sub(2, -1)})
    end
    
    local newnick = chn:sub(2, -1)
    for _chn in pairs(irc.users) do
      if irc.users[_chn].ops[usr] then
        irc.users[_chn].ops[newnick] = true
        irc.users[_chn].ops[usr] = nil
      end
      
      if irc.users[_chn].vcs[usr] then
        irc.users[_chn].vcs[newnick] = true
        irc.users[_chn].vcs[usr] = nil
      end
      
      if irc.users[_chn].amp[usr] then
        irc.users[_chn].amp[newnick] = true
        irc.users[_chn].amp[usr] = nil
      end
      
      if irc.users[_chn].tld[usr] then
        irc.users[_chn].tld[newnick] = true
        irc.users[_chn].tld[usr] = nil
      end
      
      if irc.users[_chn].all[usr] then
        irc.users[_chn].all[newnick] = true
        irc.users[_chn].all[usr] = nil
      end
    end
  end
  
  if event == "QUIT" then
    for _,fnc in pairs(_MODULES.Event.Quit) do
      fnc({user = usr,ip = ipa,message = chn:sub(2, -1)})
    end
    
    for _chn in pairs(irc.users) do
      irc.users[_chn].ops[usr] = nil
      irc.users[_chn].vcs[usr] = nil
      irc.users[_chn].tld[usr] = nil
      irc.users[_chn].amp[usr] = nil
      irc.users[_chn].all[usr] = nil
    end
  end
  
end

tcp:close()