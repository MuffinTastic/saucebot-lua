# Welcome

**Requires Lua 5.1/5.2, LuaSocket**

Welcome to Saucebot 2, an upgraded version of Saucebot, now in Lua.

This new version includes:

- A majorly improved module system

    * Module reloading

    * Better arguments system

    * Cleaner environment

- Data structures for OPed and voiced users

Please read the [Saucebot 2 Wiki](https://bitbucket.org/MuffinTastic/saucebot-lua/wiki/Home "Saucebot 2 Wiki") for more info!