local config = {}

config.host = "irc.quakenet.org"
config.port = 6667

config.nick = "Luacebot"
config.desc = "Saucebot in Lua!"
config.channels = {
  "#luacebot"
}

config.prefix  = "::"
config.modules = {
  "general"
}

return config